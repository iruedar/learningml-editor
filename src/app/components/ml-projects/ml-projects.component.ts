import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IProject } from 'src/app/interfaces/interfaces';
import { ProjectManagerService } from 'src/app/services/project-manager.service';
import { slideInAnimation } from 'src/app/animations';
import { ConfigService } from 'src/app/services/config.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-projects',
  templateUrl: './ml-projects.component.html',
  styleUrls: ['./ml-projects.component.css'],
  animations: [slideInAnimation]
})
export class MlProjectsComponent implements OnInit {

  projects = [];
  loaded = false;

  constructor(
    private projectManager: ProjectManagerService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private translate: TranslateService, ) {

    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }
  }

  ngOnInit() {
    this.projectManager.getUserProjects().subscribe(
      v => {
        this.projects = v;
        this.loaded = true;
      },
      e => {
        this.snackBar.open("tienes que iniciar sesión antes para poder ver tus proyecto en el servidor",
          'Inicia sesión antes', {
          duration: 3000,
          verticalPosition: 'top'
        });
      }
    );;
  }

  loadProject(project) {
    window.location.href = ConfigService.settings.easyml.url +
      '/model/' + project.type + '?id=' + project.id;
  }

  deleteProject(project_id) {

    let r = confirm('¿Estás seguro?');

    if (!r) return;

    this.projectManager.deleteProject(project_id).subscribe(
      v => {
        this.snackBar.open("El proyecto ha sido eliminado",
          'Eliminado!', {
          duration: 3000,
          verticalPosition: 'top'
        });
        window.location.reload();
      },
      e => {
        this.snackBar.open("No se ha podido borrar el proyecto",
          'Ooohhhh!', {
          duration: 3000,
          verticalPosition: 'top'
        });
      }
    );
  }

  showProjectForm(project) {
    const dialogRef = this.dialog.open(MlFormProjectDialogComponent, {
      width: '350px',
      data: { name: project.name, description: project.description }
    });

    dialogRef.afterClosed().subscribe(p => {
      if (p == null) return;
      let message = "";

      this.projectManager.updateProject(project.id, p).subscribe(
        v => {
          project.name = p.name;
          project.description = p.description;
        },
        e => {
        }
      );
    });
  }

  shareProject(project) {
    this.projectManager.shareProject(project).subscribe(
      (v: IProject) => {
        project.shared = v.shared;
      },
      e => {
      }
    );
  }

  unShareProject(project) {
    this.projectManager.unShareProject(project).subscribe(
      (v: IProject) => {
        project.shared = v.shared;
      },
      e => {
      }
    );
  }
}

@Component({
  templateUrl: './ml-form-project-dialog.html',
  styleUrls: ['./ml-projects.component.css']
})
export class MlFormProjectDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlFormProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IProject) { }

  close(event) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
