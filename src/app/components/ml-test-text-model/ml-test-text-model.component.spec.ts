import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlTestTextModelComponent } from './ml-test-text-model.component';

describe('MlTestTextModelComponent', () => {
  let component: MlTestTextModelComponent;
  let fixture: ComponentFixture<MlTestTextModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlTestTextModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlTestTextModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
