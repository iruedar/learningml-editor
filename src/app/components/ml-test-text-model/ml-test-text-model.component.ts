import { Component, OnInit } from '@angular/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { ScratchManagerService } from 'src/app/services/scratch-manager.service';
import { MlAlgorithmService } from 'src/app/services/ml-algorithm.service';
import { FeatureExtractionService } from 'src/app/services/feature-extraction.service';
import { TestModelService } from 'src/app/services/test-model.service';

@Component({
  selector: 'app-ml-test-text-model',
  templateUrl: './ml-test-text-model.component.html',
  styleUrls: ['./ml-test-text-model.component.css']
})
export class MlTestTextModelComponent implements OnInit {

  testText: string;
  prediction;
  mostLikelyClass: string;
  confidenceText: string;

  constructor(public labeledDataManager: LabeledDataManagerService,
    private featureExtractionService: FeatureExtractionService,
    private mlAlgorithmService: MlAlgorithmService,
    private scratchManager: ScratchManagerService,
    private testModelService: TestModelService) { }

  ngOnInit() {
  }

  test() {
    if (!this.testModelService.checkIfTestCanBeDone()) return;
    this.featureExtractionService.extractInstanceFeatures(this.testText)
      .then(inputTensor => {
        this.mlAlgorithmService.classify(inputTensor).then(prediction => {
          this.prediction = prediction;
          this.testModelService.confidenceLevel(prediction[0][1], prediction[0][0])
            .subscribe((res: string) => {
              this.confidenceText = res;
            });
        });
      });

  }

  loadScratch() {
    this.scratchManager.load();
  }
}
