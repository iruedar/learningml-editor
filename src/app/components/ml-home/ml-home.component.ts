import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { ConfigService } from 'src/app/services/config.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-home',
  templateUrl: './ml-home.component.html',
  styleUrls: ['./ml-home.component.css']
})
export class MlHomeComponent implements OnInit {

  constructor(private labeledManager: LabeledDataManagerService,
    private router: Router,
    private translate: TranslateService,
  ) {
    if (localStorage.getItem("language") == undefined) {
      localStorage.setItem("language", "es");
    }

    this.translate.setDefaultLang(localStorage.getItem("language"));
  }

  ngOnInit() {
  }

  openText() {
    if (this.labeledManager.modelType == 'image') {
      var r = confirm("Si continúas perderás los datos que no hayas guardado.");
      if (r == true) {
        window.location.href = ConfigService.settings.easyml.url + "/model/text";
      }
      return;
    }
    this.router.navigate(['model', 'text']);
  }


  openImage() {
    if (this.labeledManager.modelType == 'text') {
      var r = confirm("Si continúas perderás los datos que no hayas guardado.");
      if (r == true) {
        window.location.href = ConfigService.settings.easyml.url + "/model/image"
      }
      return;
    }
    this.router.navigate(['model', 'image']);
  }

  openNumerical(){
    if (this.labeledManager.modelType == 'numerical') {
      var r = confirm("Si continúas perderás los datos que no hayas guardado.");
      if (r == true) {
        window.location.href = ConfigService.settings.easyml.url + "/model/numerical"
      }
      return;
    }
    this.router.navigate(['model', 'numerical']);
  }

}
