import { Component, OnInit, Inject } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/models/user.model';
import { ConfigService } from 'src/app/services/config.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-ml-login',
  templateUrl: './ml-login.component.html',
  styleUrls: ['./ml-login.component.css']
})
export class MlLoginComponent implements OnInit {

  username: string;

  constructor(
    private router: Router,
    public authService: AuthenticationService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar ) {
    this.username = authService.getCurrentUsername();
  }

  ngOnInit() {
  }

  showLoginForm() {
    const dialogRef = this.dialog.open(MlFormLoginDialogComponent, {
      width: '250px',
      data: { username: "", password: "" }
    });

    dialogRef.afterClosed().subscribe(user => {
      if (user == null) return;
      let message = "";

      this.authService.login(user).subscribe(
        v => {
          this.username = v.username;
          message =  'Ahora puedes guardar tus proyectos y compartirlos con la comunidad';
          this.snackBar.open(message,
            'Bienvenido', {
            duration: 3000,
            verticalPosition: 'top'
          });
        },
        e => {
          message = "El nombre de usuario o la contraseña son incorrectos";
          this.snackBar.open(message,
            'Error', {
            duration: 3000,
            verticalPosition: 'top'
          });
        });
    });
  }

  openMyStuff(){
    this.router.navigate(['/projects']);
  }

  openSendBug(){
    this.router.navigate(['/report']);
  }

  openSharedStuff(){
    this.router.navigate(['/sharedprojects']);
  }

  showSignupForm(){
    this.router.navigate(['/signup']);
  }
  
  logout(){
    this.authService.logout();
    window.location.href = ConfigService.settings.easyml.url;
  }
}

@Component({
  templateUrl: 'ml-form-login-dialog.html',
})
export class MlFormLoginDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlFormLoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User) { }

  close(event) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}