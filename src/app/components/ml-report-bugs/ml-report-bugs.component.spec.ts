import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlReportBugsComponent } from './ml-report-bugs.component';

describe('MlReportBugsComponent', () => {
  let component: MlReportBugsComponent;
  let fixture: ComponentFixture<MlReportBugsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlReportBugsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlReportBugsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
