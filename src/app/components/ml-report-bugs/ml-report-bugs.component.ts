import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-ml-report-bugs',
  templateUrl: './ml-report-bugs.component.html',
  styleUrls: ['./ml-report-bugs.component.css']
})
export class MlReportBugsComponent implements OnInit {

  bug = {
    title: "",
    description: ""
  };

  private httpOptions = {};

  url = ConfigService.settings.api.url_base +
    ConfigService.settings.api.path;

  url_bug = this.url + 'sendbug';

  constructor(
    private router: Router,
    private translate: TranslateService,
    private httpClient: HttpClient,
    private authService: AuthenticationService
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + this.authService.getCurrentToken()
      })
    };
    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }
  }

  ngOnInit() {
  }

  sendBug() {
    if (this.bug.title == "" || this.bug.description == "") return;

    const formBug = this.httpClient.post(this.url_bug, this.bug, this.httpOptions);

    formBug.subscribe(
      v => {
        alert("¡Gracias por tu aportación! Intentaremos tenerla en cuenta para la próxima versión")
        this.router.navigate(['']);
      },
      e => { alert(e.message) });
  }

}
