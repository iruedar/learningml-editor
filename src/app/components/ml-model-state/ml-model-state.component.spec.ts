import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlModelStateComponent } from './ml-model-state.component';

describe('MlModelStateComponent', () => {
  let component: MlModelStateComponent;
  let fixture: ComponentFixture<MlModelStateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlModelStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlModelStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
