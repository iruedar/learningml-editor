import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlTestImageModelComponent } from './ml-test-image-model.component';

describe('MlTestImageModelComponent', () => {
  let component: MlTestImageModelComponent;
  let fixture: ComponentFixture<MlTestImageModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlTestImageModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlTestImageModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
