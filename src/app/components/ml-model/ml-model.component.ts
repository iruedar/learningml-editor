import { Label, ITrainResult, IProject, MLState } from 'src/app/interfaces/interfaces';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSliderChange } from '@angular/material/slider';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, Inject } from '@angular/core';
import { ShowProgressSpinnerService } from '../../services/show-progress-spinner.service';
import { ScratchManagerService } from '../../services/scratch-manager.service';
import { LabeledDataManagerService } from '../../services/labeled-data-manager.service';
import { slideInAnimation } from 'src/app/animations';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FeatureExtractionService } from 'src/app/services/feature-extraction.service';
import { MlAlgorithmService } from 'src/app/services/ml-algorithm.service';
import { from } from 'rxjs';
import { PlotService } from 'src/app/services/plot.service';
import { LmlMessageProtocolService } from 'src/app/services/lml-message-protocol.service';


type DialogData = Label;

@Component({
  selector: 'app-ml-modelo',
  templateUrl: './ml-model.component.html',
  styleUrls: ['./ml-model.component.css'],
  animations: [slideInAnimation]
})
export class MlModelComponent implements OnInit {

  graphHistory = null;
  graphConfusionMatrix = null;
  graphContour = null;
  panelOpenState = false;
  labels = new Set<Label>();
  trainResult: ITrainResult;
  isLinear = true;
  activeLang = 'es';
  cameraStatus = false;
  loading = false;
  language = 'gl';
  mlAlgorithm = 'neural-network';
  mlAlgorithmData = {
    totalNumberOfExamples: 0,
    numberOfValidationExamples: 0,
    trainingTime: null,
  }
  mode = "basic";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private authService: AuthenticationService,
    public labeledDataManager: LabeledDataManagerService,
    private scratchManager: ScratchManagerService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private progressSpinner: ShowProgressSpinnerService,
    private featureExtractionService: FeatureExtractionService,
    private mlAlgorithmService: MlAlgorithmService,
    private plotService: PlotService,
    private lmlMessageProtocol: LmlMessageProtocolService) {
    this.language = localStorage.getItem('language');
    this.labeledDataManager.mlAlgorithm = this.mlAlgorithm;
  }

  ngOnInit() {

    let ch_load_json = new BroadcastChannel('load-channel');
    ch_load_json.addEventListener('message', e => {
      this.graphConfusionMatrix = null;
      this.graphContour = null;
    });

    let ch = new BroadcastChannel('model-channel');
    ch.addEventListener('message', e => {
      if (e.data.image) {
        let img = new Image();
        img.src = e.data.data;
        img.onload = () => {
          this.labeledDataManager.addEntry(
            {
              label: e.data.label,
              data: img
            }
          );
        }
      } else {
        this.labeledDataManager.addEntry(e.data);
      }

    });

    this.lmlMessageProtocol.initServerNode();

    //this.translate.setDefaultLang(this.activeLang);
    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }

    this.route.params.subscribe(params => {
      this.labeledDataManager.modelType = params['type'];

      this.route.queryParams.subscribe(
        (v: IProject) => {
          if (v.id != undefined) {
            this.loadFromServer(v.id);
          }
        }
      );
    });
  }

  loadFromServer(id: number) {

    let keys_to_translate = [
      'model.init_sesion.title',
      'model.init_sesion.message',
    ]

    this.translate.get('model.init_sesion').subscribe((res: string) => {

    });

    if (!this.authService.isAuthenticated) {
      this.snackBar.open("tienes que iniciar sesión antes para poder guardar el proyecto en el servidor",
        'Inicia sesión antes', {
        duration: 3000,
        verticalPosition: 'top'
      });
    }
    this.loading = true;
    this.labeledDataManager.loadFromServer(id).subscribe(
      v => {

        this.loading = false;

      },
      e => {
        this.loading = false;
        let message = "Ha ocurrido un problema con el servidor, no se ha podido cargar el proyecto";
        if (e.status == 401) {
          message = "Se requiere iniciar sesión para cargar proyectos desde el servidor";
        } else if (e.status >= 402 && e.status < 500) {
          message = "Ese proyecto no existe";
        }
        this.snackBar.open(message,
          '¡Ooohhhh!', {
          duration: 3000,
          verticalPosition: 'top'
        });

      }
    )
  }

  public changeLanguage(lang) {
    this.activeLang = lang;
    this.translate.use(lang);
  }

  saveInputLabeledTexts() {
    this.labeledDataManager.save();
  }

  onInputChange(event: MatSliderChange) {
    this.labeledDataManager.state = MLState.OUTDATED
    this.labeledDataManager.mlAlgorithm = this.mlAlgorithm;
    this.graphHistory = null;
    this.graphConfusionMatrix = null;
    this.graphContour = null;
  }

  train() {

    this.labeledDataManager.mlAlgorithm = this.mlAlgorithm;

    let keys_to_translate = [
      'model.train_no_data.title',
      'model.train_no_data.message',
      'model.train_new_data.title',
      'model.train_new_data.message',
      'model.learning_from_data',
      'model.learned',
      'model.ready_to_be_used',
      'model.loading_mobilenet',
      'model.extracting_features',
      'model.features_extracted'
    ]

    this.translate.get(keys_to_translate).subscribe((res: string) => {

      if (this.labeledDataManager.state == MLState.EMPTY) {
        this.snackBar.open(res['model.train_no_data.message'],
          res['model.train_no_data.title'], {
          duration: 3000,
        });
        return;
      } else if (this.labeledDataManager.state == MLState.TRAINED) {
        this.snackBar.open(res['model.train_new_data.message'],
          res['model.train_new_data.title'], {
          duration: 3000,
        });
        return;
      }

      // Aquí comienza el proceso de construcción del modelo. 
      // primero realizamos la extracción de características (feature extraction)
      // Convierto una promesa en observable porque así puedo usar el progressSpiner
      // para avisar de que se está cargando mobilenet (en el caso de que estemos
      // con un modelo de imágenes)

      let timeStart = null;
      let features = null;
      let featurePromise = this.featureExtractionService.extractDatasetFeatures(this.language);
      this.progressSpinner
        .showProgressSpinnerUntilExecuted(from(featurePromise),
          res['model.extracting_features'], "assets/images/matrix.gif",
          res['model.features_extracted'], res['model.features_extracted']);

      let trainPromise = featurePromise.then(_features => {
        timeStart = new Date().getTime();
        this.progressSpinner.showProgressSpinnerUntilExecuted(from(trainPromise),
          res['model.learning_from_data'], "assets/images/modern-times.gif",
          res['model.learned'], res['model.ready_to_be_used'])
          features = _features;
          let params = this.labeledDataManager.algorithms_params;
          return this.mlAlgorithmService.train(_features, params);
      });

      trainPromise.then(info => {
        let timeEnd = new Date().getTime();

        this.mlAlgorithmData.trainingTime = (timeEnd - timeStart) / 1000;

        this.mlAlgorithmData.totalNumberOfExamples = this.labeledDataManager.getTotalNumberOfExamples();
        if (this.labeledDataManager.algorithms_params.validationSplit) {
          this.mlAlgorithmData.numberOfValidationExamples = Math.floor(
            this.mlAlgorithmData.totalNumberOfExamples * this.labeledDataManager.algorithms_params.validationSplit / 100);
        }

        // Plot history
        if (info.history) {
          let filters = this.labeledDataManager.algorithms_params.validationSplit == 0 ?
            [true, true] :
            [true, true, true, true]
          this.graphHistory = this.plotService.history(info, filters);
        }

        let plotPromisesArray = []
        
        // Plot confusion matriz
        if (this.labeledDataManager.algorithms_params.validationSplit > 0) {
          let p = this.mlAlgorithmService.confusionMatrix().then(r => {
            this.graphConfusionMatrix = this.plotService.confusionMatrix(r);
          });
          plotPromisesArray.push(p);
        }

        // Plot decision boundary
        if (this.labeledDataManager.modelType == 'numerical' && this.labeledDataManager.featureDimension == 2) {
          let p = this.mlAlgorithmService.decisionBoundary().then(r => {
            this.graphContour = this.plotService.contour(r);
            
          });
          plotPromisesArray.push(p);
        }

        Promise.all(plotPromisesArray).then(() => {
          this.mlAlgorithmService.disposeTensors();
        });
        
        return info;
      });
    });

  }

  addLabel() {

    let keys_to_translate = [
      'model.label_created.title',
      'model.label_created.message',
    ]

    this.labeledDataManager.featureDimensionLocked = true;

    this.translate.get(keys_to_translate).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlAddLabelDialogComponent, {
        width: '250px',
        data: '',
      });

      dialogRef.afterClosed().subscribe(label => {
        if (label == "" || label == undefined) return;
        this.labeledDataManager.addLabel(label);
        this.snackBar.open(res['model.label_created.message'] + '`' + label + '`',
          res['model.label_created.title'], {
          duration: 2000,
        });
      });
    })
  }

  onDeleted($event) {
    this.labels.delete($event);
    this.labeledDataManager.removeLabel($event);
  }

  webcamTakeSnapshot(label) {
  }

  loadScratch() {
    this.scratchManager.load();
  }

}


@Component({
  templateUrl: 'ml-add-label-dialog.html',
})
export class MlAddLabelDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlAddLabelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }

  close(event) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
