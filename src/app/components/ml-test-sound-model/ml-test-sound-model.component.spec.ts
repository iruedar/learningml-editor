import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlTestSoundModelComponent } from './ml-test-sound-model.component';

describe('MlTestSoundModelComponent', () => {
  let component: MlTestSoundModelComponent;
  let fixture: ComponentFixture<MlTestSoundModelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlTestSoundModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlTestSoundModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
