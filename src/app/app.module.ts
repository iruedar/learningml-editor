import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MlModelComponent, MlAddLabelDialogComponent } from './components/ml-model/ml-model.component';
import { MlLabelContainerComponent, MlLabelContainerDialogComponent, MlDeleteConfirmComponent } from './components/ml-label-container/ml-label-container.component';
import { MlModelToolbarComponent, MlAboutDialogComponent } from './components/ml-model-toolbar/ml-model-toolbar.component';
import { ProgressSpinnerDialogComponent } from './components/progress-spinner-dialog/progress-spinner-dialog.component';
import { ConfigService } from './services/config.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MlLoginComponent, MlFormLoginDialogComponent } from './components/ml-login/ml-login.component';
import { AuthenticationService } from './services/authentication.service';
import { MlFilemenuComponent } from './components/ml-filemenu/ml-filemenu.component';
import { MlProjectsComponent, MlFormProjectDialogComponent } from './components/ml-projects/ml-projects.component';
import { MlSignupComponent } from './components/ml-signup/ml-signup.component';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { MlSharedProjectsComponent } from './components/ml-shared-projects/ml-shared-projects.component';
import { ProjectManagerService } from './services/project-manager.service';
import { MlHomeComponent } from './components/ml-home/ml-home.component';
import { MlWebCamComponent } from './components/ml-web-cam/ml-web-cam.component';
import { MlModelStateComponent } from './components/ml-model-state/ml-model-state.component';
import { ReversePipe } from './reverse.pipe';
import { MlFooterComponent } from './components/ml-footer/ml-footer.component';
import { MlReportBugsComponent } from './components/ml-report-bugs/ml-report-bugs.component';
import { MlTestImageModelComponent } from './components/ml-test-image-model/ml-test-image-model.component';
import { MlTestTextModelComponent } from './components/ml-test-text-model/ml-test-text-model.component';
import { MlTestSoundModelComponent } from './components/ml-test-sound-model/ml-test-sound-model.component';
import { MlTestNumericalModelComponent } from './components/ml-test-numerical-model/ml-test-numerical-model.component';
import * as PlotlyJS from 'plotly.js-dist-min';
import { PlotlyModule } from 'angular-plotly.js';

PlotlyModule.plotlyjs = PlotlyJS;


export function initializeApp(appConfig: ConfigService) {
  return () => appConfig.load();
}

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MlModelComponent,
    MlLabelContainerComponent,
    MlLabelContainerDialogComponent,
    MlDeleteConfirmComponent,
    MlAddLabelDialogComponent,
    MlModelToolbarComponent,
    ProgressSpinnerDialogComponent,
    MlLoginComponent,
    MlFormLoginDialogComponent,
    MlFilemenuComponent,
    MlProjectsComponent,
    MlFormProjectDialogComponent,
    MlSignupComponent,
    MlSharedProjectsComponent,
    MlAboutDialogComponent,
    MlHomeComponent,
    MlWebCamComponent,
    MlModelStateComponent,
    ReversePipe,
    MlFooterComponent,
    MlReportBugsComponent,
    MlTestImageModelComponent,
    MlTestTextModelComponent,
    MlTestSoundModelComponent,
    MlTestNumericalModelComponent,
  ],
  entryComponents: [
    MlLabelContainerComponent,
    MlLabelContainerDialogComponent,
    MlDeleteConfirmComponent,
    MlAddLabelDialogComponent,
    ProgressSpinnerDialogComponent,
    MlFormLoginDialogComponent,
    MlFormProjectDialogComponent,
    MlAboutDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CommonModule,
    PlotlyModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    MomentDateModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatInputModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
    MatDialogModule,
    MatTooltipModule,
    MatIconModule,
    MatChipsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatSidenavModule,
    MatStepperModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/');
        },
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ConfigService,
    ProjectManagerService,
    AuthenticationService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ConfigService], multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
