import * as tf from '@tensorflow/tfjs';
import { ExampleData, TText, Label } from '../../interfaces/interfaces';

export class FeatureExtractorNumerical {

  private classes;
  private labelsWithData;

  constructor() { }

  static async Create(labelsWithData: Map<Label, ExampleData[]>) {

    const me = new FeatureExtractorNumerical();

    me.labelsWithData = labelsWithData;

    return me;
  }

  /**
 * Build an object wich maps classes names to number and the dictionary
 * which will be used to vectorize words.
 * @param {*} traindata 
 * @param {*} bow 
 */
  buildClasses(traindata) {
    this.classes = {};
    // extract all the classe (which are the labels) without
    // repetition from traindata and map to number in order to
    // feed the ANN
    let i = 0;
    for (let data of traindata) {
      if (this.classes[data.label] == undefined) {
        this.classes[data.label] = i;
        i++;
      }
    }
  }

  getRawDataset() {
    let rawDataset = [];
    for (let label of this.labelsWithData.keys()) {
      for (let data of this.labelsWithData.get(label)) {
        let e = { label: label, numbers: data }
        rawDataset.push(e)
      }
    }
    return rawDataset;
  }

  extractInstanceFeatures(text: TText): Promise<tf.Tensor> {
    let feature = text.split(",").map(v => parseFloat(v));
    let tensor_inputs = tf.cast(tf.stack(feature), 'float32');
    return new Promise((resolve, reject) => {
      let shape = tensor_inputs.shape;
      resolve(tensor_inputs.reshape([1, shape[0]]));
    })
  }

  prepareTrainData(rawDataset) {
    let traindata_for_ann = [];
    for (let data of rawDataset) {
      traindata_for_ann.push(data["numbers"].split(",").map(v => parseFloat(v)));
    }
    return traindata_for_ann;
  }

  labelOneShotEncoding(res, num_classes) {
    let vec = [];
    for (let i = 0; i < num_classes; i += 1) {
      vec.push(0);
    }
    vec[res] = 1;
    return vec;
  }

  extractDatasetFeatures(): Promise<{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }> {

    let rawDataset = this.getRawDataset();
    this.buildClasses(rawDataset);

    let traindata_for_ann = this.prepareTrainData(rawDataset);

    let labels = [];

    for (let data of rawDataset) {
      labels.push(this.labelOneShotEncoding(
        this.classes[data.label], Object.keys(this.classes).length))
    }

    let text_labels: string[] = Array.from(this.labelsWithData.keys());
    let tensor_labels = tf.cast(tf.stack(labels), 'int32');
    let tensor_inputs = tf.cast(tf.stack(traindata_for_ann), 'float32');

    return new Promise((resolve, reject) => {
      resolve({ text_labels, tensor_labels, tensor_inputs });
    })
  }
}
