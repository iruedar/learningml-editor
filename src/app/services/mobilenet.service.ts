import { Injectable } from '@angular/core';
import * as mobilenetModule from '@tensorflow-models/mobilenet';

@Injectable({
  providedIn: 'root'
})
export class MobilenetService {

  mobilenet = null;

  constructor() { }

  async get(){
    if(!this.mobilenet){
      this.mobilenet = await mobilenetModule.load();
    }
    
    return this.mobilenet;
  }
}
