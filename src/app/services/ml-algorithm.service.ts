import { Injectable } from '@angular/core';
import { FeatureExtractionService } from './feature-extraction.service';
import { LabeledDataManagerService } from './labeled-data-manager.service';
import { MlAlgorithmNeuralNetwork } from './mlAlgorithms/ml-algorithm-neural-network';
import * as tf from '@tensorflow/tfjs';
import { MlAlgorithmKNN } from './mlAlgorithms/ml-algorithm-knn';
import { MLState } from '../interfaces/interfaces';
import { PercentPipe } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class MlAlgorithmService {

  mlAlgorithm = null;
  labels = [];
  validationDataset = null;
  features = null;

  constructor(
    private labeledDataManager: LabeledDataManagerService,
    private featureExtractionFeature: FeatureExtractionService) { }

  suffle(tensor_inputs, tensor_labels) {
    let tensorInputArray = tensor_inputs.arraySync();
    let tensorLabelsArray = tensor_labels.arraySync();
    tf.util.shuffleCombo(tensorInputArray, tensorLabelsArray);
    let _tensor_inputs = tf.tensor(tensorInputArray);
    let _tensor_labels = tf.tensor(tensorLabelsArray);

    return [_tensor_inputs, _tensor_labels];
  }

  splitData(features: { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }, splitFraction: number) {
    if (splitFraction <= 0 || splitFraction >= 1) return [features, null];

    let [tensor_inputs, tensor_labels] = this.suffle(features.tensor_inputs, features.tensor_labels);

    let totalNumberOfExamples = features.tensor_inputs.shape[0];
    let numberOfValidationExamples = Math.floor(totalNumberOfExamples * splitFraction);
    let numberOfDataExamples = totalNumberOfExamples - numberOfValidationExamples

    // take the  first numberOfValidationExamples for validation
    let tensor_inputs_data = tf.slice(tensor_inputs, 0, numberOfDataExamples);
    let tensor_labels_data = tf.slice(tensor_labels, 0, numberOfDataExamples);
    let tensor_inputs_validation = tf.slice(tensor_inputs, numberOfDataExamples, numberOfValidationExamples);
    let tensor_labels_validation = tf.slice(tensor_labels, numberOfDataExamples, numberOfValidationExamples);

    this.validationDataset = { text_labels: features.text_labels, tensor_inputs: tensor_inputs_validation, tensor_labels: tensor_labels_validation }

    return [{
      text_labels: features.text_labels,
      tensor_labels: tensor_labels_data,
      tensor_inputs: tensor_inputs_data
    }, [
      tensor_inputs_validation,
      tensor_labels_validation
    ]]
  }

  confusionMatrix() {
    let resultsPromise = []
    let numberOfSamples = this.validationDataset.tensor_inputs.shape[0];
    for (let i = 0; i < numberOfSamples; i++) {
      resultsPromise.push(this.classify(this.validationDataset.tensor_inputs.gather([i])))
    }

    let classificationResults = []
    return Promise.all(resultsPromise).then(results => {
      classificationResults = results.map(r => r[0][0]);
      let realResults = this.validationDataset.tensor_labels.arraySync()
        .map(r => this.validationDataset.text_labels[r.indexOf(1)]);

      this.validationDataset.tensor_inputs.dispose();
      this.validationDataset.tensor_labels.dispose();
      let result = { text_labels: this.validationDataset.text_labels, classified: classificationResults, real: realResults };
      return result;
    });
  }

  decisionBoundary() {

    let size = 30;
    let width_ampl = 0.1;
    let height_ampl = 0.1;

    let feature1Min = this.features.tensor_inputs.min(0).arraySync()[0];
    let feature1Max = this.features.tensor_inputs.max(0).arraySync()[0];
    let feature2Min = this.features.tensor_inputs.min(0).arraySync()[1];
    let feature2Max = this.features.tensor_inputs.max(0).arraySync()[1];

    let width = width_ampl * (feature1Max - feature1Min);
    let height = height_ampl * (feature2Max - feature2Min);

    feature1Min -= width;
    feature1Max += width;
    feature2Min -= height;
    feature2Max += height;

    let delta_x = (feature1Max - feature1Min) / size;
    let delta_y = (feature2Max - feature2Min) / size;

    let x = new Array(size), y = new Array(size), z = new Array(size);

    for (let i = 0; i < size; i++) {
      x[i] = feature1Min + delta_x*i;
      y[i] = feature2Min + delta_y*i;
      z[i] = new Array(size);
    }

    let resultsPromise = [];
    for (let i = 0; i < size; i++) {
      for (let j = 0; j < size; j++) {
        let dataInput = `${x[i]},${y[j]}`;
        let p = this.featureExtractionFeature.
          extractInstanceFeatures(dataInput).then(r => {
            return this.classify(r).then(prediction => {
              let classIndex = this.features.text_labels.indexOf(prediction[0][0]);
              z[j][i] = classIndex;
              return classIndex;
            })
          });
        resultsPromise.push(p);
      }
    }

    return Promise.all(resultsPromise).then(result => {
      return {x, y, z};
    })
  }

  disposeTensors() {
    //this.mlModel.save('downloads://image-model');
    this.features.tensor_inputs.dispose();
    this.features.tensor_labels.dispose();
  }

  train(features: { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }, params): Promise<any> {
    this.labels = features.text_labels;
    this.features = features;
    switch (this.labeledDataManager.mlAlgorithm) {
      case 'naive-bayes':
        break;
      case 'knn':
        this.mlAlgorithm = new MlAlgorithmKNN(this.labeledDataManager, params);
        break;
      case 'neural-network':
        this.mlAlgorithm = new MlAlgorithmNeuralNetwork(this.labeledDataManager, params);
    }

    let splittedData = this.splitData(features, params.validationSplit / 100);

    let featuresForTraining = splittedData[0];
    let featuresForValidation = splittedData[1];

    return this.mlAlgorithm.train(featuresForTraining, featuresForValidation)
      .then(result => {
        this.labeledDataManager.state = MLState.TRAINED;

        localStorage.setItem('modelType', this.labeledDataManager.modelType);

        return result.info;
      })
  }

  classify(input: tf.Tensor): Promise<[string, number][]> {
    let labels = this.labels;
    return this.mlAlgorithm.classify(input, labels).then(result => {
      let prediction = [];
      for (let p in result) {
        prediction.push([result[p][0], result[p][1]]);
      }

      prediction.sort((a, b) => {
        return b[1] - a[1];
      });

      return prediction
    });
  }


}
