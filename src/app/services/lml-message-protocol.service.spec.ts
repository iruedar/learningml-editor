import { TestBed } from '@angular/core/testing';

import { LmlMessageProtocolService } from './lml-message-protocol.service';

describe('LmlMessageProtocolService', () => {
  let service: LmlMessageProtocolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LmlMessageProtocolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
