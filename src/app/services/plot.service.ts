import { Injectable } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { LabeledDataManagerService } from './labeled-data-manager.service';

@Injectable({
  providedIn: 'root'
})
export class PlotService {
  colorscale = null;

  constructor(private labeledDataManager: LabeledDataManagerService) { 
    //this.colorscale =  [[0, 'rgb(166,206,227)'], [1, 'rgb(31,120,180)'], [2, 'rgb(178,223,138)'], [3, 'rgb(51,160,44)'], [4, 'rgb(251,154,153)'], [5, 'rgb(227,26,28)']];
    this.colorscale =  [[0, 'rgb(166,206,227)'], [0.25, 'rgb(31,120,180)'], [0.45, 'rgb(178,223,138)'], [0.65, 'rgb(51,160,44)'], [0.85, 'rgb(251,154,153)'], [1, 'rgb(227,26,28)']];

  }

  history(info: tf.History, filters) {
    let data = [];

    let traces = ["acc", "loss", "val_acc", "val_loss"]
    let _traces = []
    for (let index in filters) {
      if (filters[index]) _traces.push(traces[index])
    }

    for (let trace of _traces) {
      let xs = [];
      let ys = [];
      for (let indexEpoch in info.epoch) {
        xs.push(info.epoch[indexEpoch]),
        ys.push(info.history[trace][indexEpoch])
      }

      data.push({
        x: xs,
        y: ys,
        type: 'scatter',
        name: trace
      })
    }
  
    return {
      data: data,
      layout: {
         width: 600, 
         height: 400, 
         title: 'Learning evolution',
         xaxis: {
          title: 'Epoch'
        },
        yaxis: {
          title: 'Acc. / Loss/'
        } }
    };

  }

  transpose = m => m[0].map((x,i) => m.map(x => x[i]))
  
  confusionMatrix(validationData){

    let multi = [];
    for (let label of validationData.text_labels) {

      let series = [];
      
      //let reversedLabels = validationData.text_labels.map(x => x);
      //reversedLabels.reverse();
      for (let label2 of validationData.text_labels) {
        
        let numberOfLabel1ClassifiedAsLabel2 = validationData
          .real.map((v, i) => v == label && label2 == validationData.classified[i])
          .reduce((previous, current) => previous + current)

        series.push(numberOfLabel1ClassifiedAsLabel2);
          
      }
      multi.push(series);
    }

    let annotations = [];
    for ( let i = 0; i < validationData.text_labels.length; i++ ) {
      for ( let j = 0; j < validationData.text_labels.length; j++ ) {
        var textColor = 'red';
        
        var result = {
          xref: 'real',
          yref: 'classified',
          x: validationData.text_labels[i],
          y: validationData.text_labels[j],
          text: multi[i][j],
          font: {
            family: 'Arial',
            size: 12,
            color: textColor
          },
          showarrow: false,
          
        };
        annotations.push(result);
      }
    }

    let data = [
      {
        //z: [[1, 20, 30], [20, 1, 60], [30, 60, 1]],
        z: this.transpose(multi),
        x: validationData.text_labels,
        // para que no se haga el reverse in place
        y: validationData.text_labels,
        type: 'heatmap',
        colorscale: 'Greens'
      }
    ];

    return {
      data: data,
      layout: {
        width: 600, 
        height: 400, 
        title: 'Confusion matrix',
        annotations: annotations
      }
    };
  }


  scatter(): any[]{
    let labelsWithData = this.labeledDataManager.labelsWithData;

    let traces = [];
    let i = 0;
    for(let label of labelsWithData){
      let data = {
        x: [], 
        y: [],
        marker: {
          color: this.colorscale[i][1],
          line: {
            color: 'black',
            width: 1
          }
        }, 
        mode: 'markers',
        type: 'scatter', 
        name: label[0]
      };
      for (let value of label[1]){
        let sValue = value as string;
        let coord = sValue.split(",");
        data.x.push(parseFloat(coord[0]));
        data.y.push(parseFloat(coord[1]));
      }
      traces.push(data);
      i++;
    }

    return traces;
  }

  contour(mesh){
    
    mesh["type"] = 'contour';
    mesh["colorscale"] = 'Greens';
    mesh["showscale"] = false;

    let traces = this.scatter();

    traces.push(mesh);

    let layout = {
      title: 'Decision Boundary'
    };

    return {
      data: traces,
      layout: layout
    }
  }
}
