import * as tf from '@tensorflow/tfjs';
import * as knnClassifier from '@tensorflow-models/knn-classifier';
import { MLState } from '../../interfaces/interfaces';

export class MlAlgorithmKNN {

    mlModel = null;
    features = null;
    labeledDataManager = null;
    params = null;

    constructor(labeledDataManager, params) {
        this.labeledDataManager = labeledDataManager;
        this.params = params
    }

    buildModel(features: { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }) {
        this.mlModel = knnClassifier.create();
        let labels = tf.unstack(features.tensor_labels);
        let inputs = tf.unstack(features.tensor_inputs);

        for (let index in labels) {
            this.mlModel.addExample(inputs[index], labels[index]);
        }
        return true
    }

    train(features: { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }): Promise<any> {
        this.mlModel = knnClassifier.create();

        let getClassIndex = (label: tf.Tensor) => {
            let l = label.arraySync();
            //necesario para que no me diga que indexOf no es un método de l.
            l = l as number[];
            return l.indexOf(1);
        }

        let _labels: tf.Tensor[] = tf.unstack(features.tensor_labels);
        let labels = []
        for (let label of _labels) {
            labels.push(getClassIndex(label));
        }

        let inputs = tf.unstack(features.tensor_inputs);

        for (let index in labels) {
            this.mlModel.addExample(inputs[index], labels[index]);
        }
        this.labeledDataManager.state = MLState.TRAINED;

        this.mlModel.save = function () {
            let dataset = this.getClassifierDataset()
            var datasetObj = {}
            Object.keys(dataset).forEach((key) => {
                let data = dataset[key].dataSync();
                // use Array.from() so when JSON.stringify() it covert to an array string e.g [0.1,-0.2...] 
                // instead of object e.g {0:"0.1", 1:"-0.2"...}
                datasetObj[key] = Array.from(data);
            });
            let jsonStr = JSON.stringify(datasetObj)
            //can be change to other source
            localStorage.setItem("knn-model", jsonStr);
        }

        this.mlModel.load = function () {
            //can be change to other source
            let dataset = localStorage.getItem("knn-model")
            let tensorObj = JSON.parse(dataset)
            //covert back to tensor
            Object.keys(tensorObj).forEach((key) => {
                tensorObj[key] = tf.tensor(tensorObj[key], [tensorObj[key].length / 1000, 1000])
            })
            this.setClassifierDataset(tensorObj);
        }

        let result = {
            mlModel: this.mlModel,
            info: true
        }
        return new Promise((resolve, reject) => { resolve(result) })

    }

    classify(input_tensor: tf.Tensor, labels: string[]): Promise<[string, number][]> {
        let k = this.params.knn.k;
        return this.mlModel.predictClass(input_tensor, k).then(prediction => {
            let results = [];
            for (let i in prediction.confidences) {
                results.push([labels[i], prediction.confidences[i]]);
            }
            results.sort((a, b) => b[1] - a[1]);
            input_tensor.dispose();
            return results;
        })
    }
}