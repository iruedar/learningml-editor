import { TestBed } from '@angular/core/testing';

import { MlAlgorithmService } from './ml-algorithm.service';

describe('MlAlgorithmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MlAlgorithmService = TestBed.get(MlAlgorithmService);
    expect(service).toBeTruthy();
  });
});
