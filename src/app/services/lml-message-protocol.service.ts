import { Injectable } from '@angular/core';
import { MlAlgorithmService } from 'src/app/services/ml-algorithm.service';
import { FeatureExtractionService } from 'src/app/services/feature-extraction.service';
import { TestModelService } from 'src/app/services/test-model.service';
import { LabeledDataManagerService } from './labeled-data-manager.service';
import { ILabeledData, Label, TText } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class LmlMessageProtocolService {

  channel: {
    request: BroadcastChannel,
    response: BroadcastChannel
  };

  constructor(
    private featureExtractionService: FeatureExtractionService,
    private mlAlgorithmService: MlAlgorithmService,
    private testModelService: TestModelService,
    private labeledDataManager: LabeledDataManagerService
  ) {

    this.channel = {
      request: new BroadcastChannel('channel-request'),
      response: new BroadcastChannel('channel-response')
    }
  }

  _classify(data: string | HTMLImageElement): Promise<string> {
    return this.featureExtractionService.extractInstanceFeatures(data)
      .then(inputTensor => {
        return this.mlAlgorithmService.classify(inputTensor).then(prediction => {
          return this.testModelService.mostLikelyClassification(prediction);
        })
      });
  }

  _confidence(data: string | HTMLImageElement) {
    return this.featureExtractionService.extractInstanceFeatures(data)
      .then(inputTensor => {
        return this.mlAlgorithmService.classify(inputTensor).then(prediction => {
          return this.testModelService.mostLikelyConfidence(prediction);
        })
      });
  }

  classify_text(text: string): Promise<string> {
    return this._classify(text);
  }

  confidence_text(text: string): Promise<string> {
    return this._confidence(text);
  }

  classify_image(base64Image: string): Promise<string> {
    let image = document.createElement('img');

    image.src = base64Image;

    return this._classify(image);
  }

  confidence_image(base64Image: string): Promise<string> {
    let image = document.createElement('img');

    image.src = base64Image;

    return this._confidence(image);
  }

  checkNumerical(numbers: string) {
    let re = /^(\s*-?\d+(\.\d+)?)(\s*,\s*-?\d+(\.\d+)?)*$/;
    let inputIsOk = re.test(numbers);
    let featureDimension = numbers.split(",").length;

    return inputIsOk && (featureDimension == this.labeledDataManager.featureDimension)
  }

  classify_numerical(numbers: string): Promise<string> {
    if (!this.checkNumerical(numbers))
      return new Promise(resolve => { resolve("BAD DIMENSION") });
    return this._classify(numbers);
  }

  confidence_numerical(numbers: string): Promise<string> {
    if (!this.checkNumerical(numbers))
      return new Promise(resolve => { resolve("BAD DIMENSION") });
    return this._confidence(numbers);
  }

  addTextToLabel(text: string, label: string): Promise<boolean> {

    this.labeledDataManager.addEntry({ data: text, label: label });

    return new Promise(resolve => { resolve(true) });
  }

  learnFromTextsAndWait() {

    let language = "es";

    return this.featureExtractionService.extractDatasetFeatures(language)
      .then(features => {
        return this.mlAlgorithmService.train(features, this.labeledDataManager.algorithms_params)
          .then(info => {
            return true;
          })
       
      });
  }

  initServerNode() {

    this.channel.request.addEventListener('message', message => {
      let result_promise = null;
      let type = null;
      let message_id = message.data.message_id;
      switch (message.data.operation) {
        case 'classify_text':
          result_promise = this.classify_text(message.data.args[0])
          type = "string";
          break;
        case 'confidence_text':
          result_promise = this.confidence_text(message.data.args[0])
          type = "string";
          break;
        case 'classify_image':
          result_promise = this.classify_image(message.data.args[0])
          type = "string";
          break;
        case 'confidence_image':
          result_promise = this.confidence_image(message.data.args[0])
          type = "string";
          break;
        case 'classify_numerical':
          result_promise = this.classify_numerical(message.data.args[0])
          type = "string";
          break;
        case 'confidence_numerical':
          result_promise = this.confidence_numerical(message.data.args[0])
          type = "string";
          break;
        case 'add_text_to_label':
          result_promise = this.addTextToLabel(message.data.args[0], message.data.args[1])
          type = "boolean";
          break;
        case 'learn_from_texts_and_wait':
          result_promise = this.learnFromTextsAndWait();
          type = "boolean";
          break;
      }

      result_promise.then(result => {
        let response = {
          result: result,
          type: type,
          message_id: message_id
        }
        this.channel.response.postMessage(response);
      })

    })
  }
}
