import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MlModelComponent } from './components/ml-model/ml-model.component';
import { MlProjectsComponent } from './components/ml-projects/ml-projects.component';
import { MlSignupComponent } from './components/ml-signup/ml-signup.component';
import { MlSharedProjectsComponent } from './components/ml-shared-projects/ml-shared-projects.component';
import { MlHomeComponent } from './components/ml-home/ml-home.component';
import { MlReportBugsComponent } from './components/ml-report-bugs/ml-report-bugs.component';


const routes: Routes = [
  {path: '', component: MlHomeComponent, data: {animation: 'Home'}},
  {path: 'model/:type', component: MlModelComponent, data: {animation: 'Home'}},
  {path: 'projects', component: MlProjectsComponent, data: {animation: 'Project'}},
  {path: 'signup', component: MlSignupComponent, data: {animation: 'Signup'}},
  {path: 'sharedprojects', component: MlSharedProjectsComponent, data: {animation: 'Shared'}},
  {path: 'report', component: MlReportBugsComponent, data: {animation: 'Shared'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
