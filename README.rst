LearningML
##########

Introduction
************

LearningML is an educative platform intended to teach and learn Machine Learning
(ML) fundamentals through hand-on activities. The full platform is composed of
four parts:

- a web site (https://learnigml.org)
- a Machine Learning editor (this repo contains the code)
- a programming editor, which is a modification of the Scratch project
      (https://scratch.mit.edu/) (https://gitlab.com/learningml/learningml-scratch-gui, https://gitlab.com/learningml/learningml-scratch-vm, https://gitlab.com/learningml/learningml-scratch-l10)
- an API server application (https://gitlab.com/learningml/learningml-api).

The  ML editor allows to build ML models from labeled datasets of texts or images.
The programming editor allows to code applications able to use these ML models.
And the server application is intended to register users and allows them to 
save and share their projects. 

The Machine Learning editor is independent and can be run alone. However if
you want to code an application that uses the ML models, you have to use the 
programming editor. Also, if you want to allow user registration and project 
saving and sharing, you will have to use the server application. 

The main design principle followed when developing LearningML has been simplicity.
The main goal has been to build a tool very easy to start with as well as getting
some results quickly and easily. Hence, it is a platform specially appropriate to 
learn and teach ML fundamentals to children.

LearningML is also a computing education research project aiming to discover
effective ways to learn and teach artificial intelligence and Machine Learning
fundamentals at school.

And, finally, LearningML is an open source project wishing to grow with the help
of everyone interested in contributing. 

You can install the ML editor, the programming editor and the api server in your
computer, study the code and modify it in the terms gathered in their licenses 
(Affero GNU for the ML editor and BSD-3 for the programming editor).

There is also a cloud instance of the platform freely (as in gratis) available
to everybody who wants to use it. You can get access by pointing your browser to
https://learnigml.org.

You can find more information about LearningML project in the web site 
(https://learnigml.org).

Research papers on LearningML
*****************************

Some papers have been written around LearningML:

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles,
  G. Evaluation of an Online Intervention to Teach Artificial Intelligence With
  LearningML to 10-16-Year-Old Students.
  https://www.researchgate.net/publication/344744220_Evaluation_of_an_Online_Intervention_to_Teach_Artificial_Intelligence_With_LearningML_to_10-16-Year-Old_Students

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles,
  G. (2020, October). Introducing Artificial Intelligence Fundamentals with 
  LearningML: Artificial Intelligence made easy. In Eighth International 
  Conference on Technological Ecosystems for Enhancing Multiculturality 
  (pp. 18-20).
  https://www.researchgate.net/publication/348697319_Introducing_Artificial_Intelligence_Fundamentals_with_LearningML_Artificial_Intelligence_made_easy

- Rodríguez-García, J. D., Moreno-León, J., Román-González, M., & Robles, G. (2020). 
  LearningML: A Tool to Foster Computational Thinking Skills through Practical 
  Artificial Intelligence Projects. Revista de Educación a Distancia (RED),
  20(63).
  https://revistas.um.es/red/article/view/410121

The first paper shows an experimental study supporting that LearningML helps
to learn AI fundamentals to children in the age 10-16. The second one gives a
brief description of the platform. And the last one presents LearningML in depth.

Features
********

- Gather image/text training datasets.
- Build Machine Learning image/text recognition models (with neural network
  algorithms).
- Evaluation of built models.
- Exporting ML models to Scratch.
- Saving locally the training dataset.
- Loading training datasets from your computer.
- Create user accounts (with the API server application).
- Saving training datasets in the cloud (with the API server application).
- Sharing training datasets in the cloud (with the API server application).
- (planned) sound recognition.
- (planned) tabular datasets management.
- (planned) Machine Learning algorithm selection.
- (planned) Configuring relevant parameters of the selected Machine Learning
   algorithm.
- (planned) Confusion matrix to enrich evaluation phase. 
- (planned) Showing learning evolution trough plots.
- (planned) Saving and loading ML models.
- (planned) Desktop (offline) version.

Requirements
************

The LearningML editor is a cross platform web application, Therefore, it can 
be deployed on a modern computer running:
- Linux, 
- MacOS or,
- Windows OS.

Also, you will need:

- Node.js runtime environment (v14.16.0) (https://nodejs.org/)
- Angular CLI (v8.3.19) (https://cli.angular.io/)
- A standard web browser (Chrome or Firefox recommended)

Getting started
***************

In order to install a local copy of Learning editor, follow the following steps.

1. Install node.js runtime environment.
2. Install Angular CLI.
3. Open a terminal and clone this repo:
    # git clone https://gitlab.com/learningml/learningml-editor.git
    # cd learningml-editor
4. Install dependencies:
    # npm install
5. Run development server
   # ng serve
6. Open a web browser pointing to `http://localhost:4200` to access the learning
   editor application.

You can test the application following these steps (we assume that you have 
chosen the English language):

1. Click on "recognize images" button.
2. Click on "Add new class of images" button and enter the label "face".
3. Click on "Add new class of images" button again and enter de label "hand"
4. Click in the button located in "hand" label with a camera image and add some 
   pictures of your face. When you finish, close the camera by clicking on 
   the button with an "X".
5. Click in the button located in "face" label with a camera image and add some 
   pictures of your face. When you finish, close the camera by clicking on 
   the button with an "X".
6. Click on "Learning to recognize  images" button, and wait until the learning
   phase is finished. 
7. Click on the button with a camera located in the third column ("Try") and
   take a picture of your face or your hand. The model will be give as output
   the probability of being a hand or a face.

User manual
***********

The LearningML editor is very intuitive and building ML models is straightforward:
you create some classes and add example data to them. Afterwards, the 
Machine Learning algorithm is run to build the ML model. Finally you can
evaluate this model by giving new data as input and checking the classification
thrown as output.

You can find more details in the user manual
[https://web.learningml.org/wp-content/uploads/2020/05/Manual_LearningML.pdf]

The manual is written in Spanish, but its translation to English is planned as 
part of the future development.

Build
*****

Run `ng build` to build the project. The build artifacts will be stored in the
`dist/` directory. Use the `--prod` flag for a production build.

If you are going to deploy the code in a web server under a route distinct to 
`/`, you must specify it with the option `--base-href`. For instance, if the 
route is `/editor/`, you can build a deployment directory as follows:

    # ng build --base-href /editor/ --configuration production

Docker
******

LearningML-editor together with lml-scratch can be run in a docker container.

    # docker run -d -p 8080:80  juandalibaba/learningml

Running unit tests
******************

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Running end-to-end tests
************************

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


Appendix A: Reverse proxy with nginx to run the ML editor together with the programming editor
*********************************************************************************************

LearningML editor uses tensorflow.js and brain.js to build the ML models, which
are saved locally in the browser once they have been built by using of 
`indexeddb`. Afterwards, our version of Scratch can read the model from
`indexeddb`. But `indexeddb` can only be shared among applications running in 
the same domain. The problem is that in some environments this is not the case.
For instance in a development environment each application runs on a different
port, and they are considered as different domains.

This problem can be fixed by means of a reverse proxy with nginx which allows 
to access to both applications from the same domain. For instance, in a 
development environment where LearningML editor runs in port 4200 and Scratch runs
in 8601 port, the following configuration of nginx does the trick:

    server {
        listen       8080;
        server_name  localhost;

        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_buffering off;

        location / {
            proxy_pass http://localhost:4200;
        }
        location /scratch/ {
            proxy_pass http://localhost:8601/;
        }
    }

Once the nginx service is started you can access to LearningML editor from:

    http://localhost:8080/learningml

And to Scratch from:

    http://localhost:8080/scratch

If you choose to serve LearningML editor from a route different to `/`, as in
the example, you must start the development server like this:

    # ng serve --base-href /learningml/ --live-reload=false 

Affero GPL License
***********************

LearningML, the easiest way to learn Machine Learning fundamentals

Copyright (C) 2020 Juan David Rodríguez García & KGB-L3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
